import sys
import pprint
import requests
import datetime
from bs4 import BeautifulSoup as BS4


url = sys.argv[1]
# url = "https://www.multirede.com.br/2018/04/25/aplicacoes-em-nuvem/"
hdr = {'User-Agent': 'Mozilla/5.0'}
r = requests.get(url, headers=hdr)

soup = BS4(r.content, 'html.parser')
# html = list(soup.children)
# l = list(html.children)
#
# head = l[1]
# body = l[3]
#
# l = list(head.children)

title = soup.find_all('title')
title = title[0].get_text()

today = datetime.datetime.now()
ref = title + " Disponível em: <" + url + ">. Acesso em: " + today.strftime("%d %b. %Y") + "."
print(str(ref))
