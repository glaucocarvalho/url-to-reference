=== Conversor de link para referência formal ===
O conversor de link para referência formal pega um link de um site qualquer e converte para o modelo de referência formal da
Univercidade do Estado do Rio de Janeiro.

== getting started ==
python3 reference_generator.py link

Onde link vai ser o link copiado diretamente da url.
